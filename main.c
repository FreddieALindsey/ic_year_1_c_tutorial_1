#include <stdio.h>
#include <stdlib.h>

static int* readGuess(void) {
  char buffer[4];
  fgets(buffer, 4, stdin);
  int guess[4];
  int i;
  for (i = 0; i < 4; ++i) {
    guess[i] = atoi(&buffer[i]);
  }
  int *guessptr = guess;
  return guessptr;
}

static int blackScore(int* guess, int* code) {

  return 0;
}

static int whiteScore(int* guess, int* code) {

  return 0;
}

static void printScore(int* g, int* c) {
  printf("(%2i, %2i)\n", blackScore(g, c), whiteScore(g,c));
}

static void usage(void) {
  printf("Welcome to the game. Type in your guess:\n");
}

int main(int argc, char** argv) {
  int codes_length = 5;
  int code_length = 4;
  int codes[][4] = {
    {1,8,9,2}, {2,4,6,8}, {1,9,8,3}, {7,4,2,1}, {4,6,8,9}
  };

  int i = 0;
  usage();
  while (i < codes_length) {
    int* guess = readGuess();
    while (blackScore(guess, codes[i]) != code_length) {
      printScore(guess, codes[i]);
      guess = readGuess();
    }

    printf("You have guessed correctly!\n");
    if (i < codes_length) {
      printf("Would you like another game? [y/n]\n");
      char ans = fgetc(stdin);
      while (ans != 'y' || ans != 'n') {
        printf("Response not understood. Please try again.\n"
               "Would you like another game? [y/n]\n");
        ans = fgetc(stdin);
      }
      if (ans == 'y') break;
    }
    ++i;
  } 
  
  return EXIT_SUCCESS;
}

